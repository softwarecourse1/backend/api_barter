/*jshint esversion: 6 */

const Course = require(`${config.path.model}/course`);
const Episode = require(`${config.path.model}/episode`);
const User = require(`${config.path.model}/user`);
const Category = require(`${config.path.model}/category`);
const City = require(`${config.path.model}/city`);

module.exports = class Controller {
	constructor() {
		this.model = { Course, Episode, User , City , Category};
	}

	showValidationErrors(req, res) {
		let errors = req.validationErrors();
		if (errors) {
			res.status(422).json({
				message: errors.map(error => {
					return {
						field: error.param,
						message: error.msg
					};
				}),
				success: false
			});
			return true;
		}
		// return new Promise((resolve, reject) => {
		// 	let errors = req.validationErrors();
		// 	if (errors) {
		// 		res.status(422).json({
		// 			message: errors.map(error => {
		// 				return {
		// 					field: error.param,
		// 					message: error.msg
		// 				};
		// 			}),
		// 			success: false
		// 		});
		// 		reject();
		// 	}
		// 	resolve();
		// });
	}

	escapeAndTrim(req, items) {
		items.split(' ').forEach(item => {
			req.sanitize(item).escape();
			req.sanitize(item).trim();
		});
	}
};
