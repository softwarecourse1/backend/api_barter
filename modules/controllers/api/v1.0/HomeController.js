module.exports = new (class HomeController {
	index(req, res) {
		res.status(200).json('Home Controller...');
	}

	version(req, res) {
		res.json('version 1.');
	}
})();
