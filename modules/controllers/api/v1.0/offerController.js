
/*jshint esversion: 6 */


const Controller = require(`${config.path.controller}/Controller`);

module.exports = new (class offerController extends Controller {

	registerOffer(req, res) {
		// // Validation
		// req.checkBody('id', 'لطفا ایمیل صحیح وارد کنید.').notEmpty();
		// req.checkBody('title', 'لطفا ایمیل صحیح وارد کنید.').notEmpty();
		// req.checkBody('icon', 'لطفا پسورد را وارد کنید.').notEmpty();

		// // Show errors
		// if (this.showValidationErrors(req, res)) {
		// 	return;
		// }
		
		const array = req.body.offer_pairs

		array.forEach(element => {
			this.model.Pair({
				cat_id: element.pair,
				pair_title: element.pair_title,
				pair_advantage: element.pair_advantage,
			})
		});


		this.model
			.Offer({
				
				offer_cat_id: req.body.offer_cat_id,
				offer_description: req.body.offer_description,
				offer_title: req.body.offer_title,
				offer_image_id: req.body.offer_image_id,
				offer_location_id: req.body.offer_location_id,
				offer_username: req.body.offer_username,
				offer_pairs: req.body.offer_pairs
			})
			.save(err => {
				if (err) {
					if (err.code == 11000) {
						return res.status(402).json({
							data: 'duplicate email',
							success: false
						});
					}
				}

				res.status(200).json({
					//body: 
					data: 'register successfull',
					success: true
				});
			});
		// Create user
	}

})();
