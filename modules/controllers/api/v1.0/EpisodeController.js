/*jshint esversion: 6 */

const Controller = require(`${config.path.controller}/Controller`);
const EpisodeTransform = require(`${config.path.transform}/V1.0/EpisodeTransform`);

module.exports = new (class EpisodeController extends Controller {
	single(req, res) {
		req.checkParams('id', 'لطفا آی‌دی صحیح را وارد کنید.').isMongoId();
		if (this.showValidationErrors(req, res)) {
			return;
		}

		this.model.Episode.findById(req.params.id)
			.populate('course')
			.exec((err, episode) => {
				if (err) throw err;

				if (episode) {
					res.status(200).json({
						data: new EpisodeTransform().transform(episode),
						successful: true
					});
				} else {
					res.status(404).json({
						data: 'Not found',
						successful: false
					});
				}
			});
	}
})();
