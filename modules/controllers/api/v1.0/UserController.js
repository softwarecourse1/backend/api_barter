/*jshint esversion: 6 */

const bcrypt = require('bcrypt');
const Controller = require(`${config.path.controller}/Controller`);
const UserTransform = require(`${config.path.transform}/V1.0/UserTransform`);

module.exports = new (class UserController extends Controller {
	index(req, res) {
		return res.json({
			data: new UserTransform().transform(req.user)
		});
	}

	uploadImage(req, res) {
		if (req.file) {
			res.json({
				message: 'upload successfully',
				data: {
					imagePath: 'http://localhost:8001/' + req.file.path.replace(/\\/g, '/')
				},
				success: true
			});
		} else {
			res.json({
				message: 'upload not successfully',
				success: false
			});
		}
	}
})();
