/*jshint esversion: 6 */

const Controller = require(`${config.path.controller}/Controller`);
const EpisodeTransform = require(`${config.path.transform}/V1.0/EpisodeTransform`);

module.exports = new (class EpisodeController extends Controller {
	index(req, res) {
		const page = req.query.page || 1;
		this.model.Episode.paginate({}, { page, limit: 2 })
			.then(result => {
				if (result)
					return res.json({
						data: new EpisodeTransform().withPaginate().transformCollection(result),
						success: true
					});
			})
			.catch(err => console.log(err));

		// this.model.Episode.find({}, (err, episodes) => {
		// 	if (err) throw err;
		// 	if (episodes)
		// 		return res.json({
		// 			data: episodes,
		// 			success: true
		// 		});
		// });
	}

	single(req, res) {
		req.checkParams('id', 'لطفا آی‌دی صحیح را وارد کنید.').isMongoId();
		if (this.showValidationErrors(req, res)) {
			return;
		}
		this.model.Episode.findById(req.params.id, (err, episode) => {
			if (err) throw err;

			if (episode) {
				return res.status(200).json({
					data: episode,
					success: false
				});
			} else {
				return res.status(404).json({
					data: 'Not found',
					success: false
				});
			}
		});
	}

	store(req, res) {
		// Validation
		req.checkBody('course_id', 'لطفا آی‌دی صحیح را وارد کنید.').isMongoId();
		req.checkBody('title', 'عنوان نمیتواند خالی بماند').notEmpty();
		req.checkBody('body', 'متن نمیتواند خالی بماند').notEmpty();
		req.checkBody('videoUrl', 'قیمت نمیتواند خالی بماند').notEmpty();
		req.checkBody('number', 'عنوان نمیتواند خالی بماند').notEmpty();

		this.escapeAndTrim(req, 'course_id title body number');

		if (this.showValidationErrors(req, res)) return;

		this.model.Course.findById(req.body.course_id, (err, course) => {
			let newEpisode = new this.model.Episode({
				course: course._id,
				title: req.body.title,
				body: req.body.body,
				videoUrl: req.body.videoUrl,
				number: req.body.number
			});
			newEpisode.save(err => {
				if (err) throw err;

				course.episodes.push(newEpisode._id);
				course.save();

				return res.status(200).json({
					data: 'Episode created successfully',
					success: true
				});
			});
		});
	}

	update(req, res) {
		req.checkParams('id', 'لطفا آی‌دی صحیح را وارد کنید.').isMongoId();
		if (this.showValidationErrors(req, res)) {
			return;
		}
		this.model.Episode.findByIdAndUpdate(
			req.params.id,
			{
				title: req.body.title
			},
			(err, episode) => {
				if (err) throw err;
				if (episode) {
					return res.status(200).json({
						data: 'update succesfull',
						success: true
					});
				} else {
					return res.status(404).json({
						data: 'not exist',
						success: false
					});
				}
			}
		);
	}

	destroy(req, res) {
		req.checkParams('id', 'لطفا آی‌دی صحیح را وارد کنید.').isMongoId();
		if (this.showValidationErrors(req, res)) {
			return;
		}
		// this.model.Episode.findByIdAndRemove(req.params.id, (err, episode) => {
		// 	if (err) throw err;
		// 	if (episode) {
		// 		res.status(200).json({
		// 			data: 'delete succesfull',
		// 			success: true
		// 		});
		// 	} else {
		// 		res.status(404).json({
		// 			data: 'not exist',
		// 			success: false
		// 		});
		// 	}
		// });

		this.model.Episode.findById(req.params.id)
			.populate('course')
			.exec((err, episode) => {
				if (err) throw err;
				if (episode) {
					let course = episode.course;
					let position = course.episodes.indexOf(episode._id);
					course.episodes.splice(position, 1);
					course.save(err => {
						if (err) throw err;
						episode.remove();
						return res.status(200).json({
							data: 'delete succesfull',
							success: true
						});
					});
				} else {
					return res.status(404).json({
						data: 'not exist',
						success: false
					});
				}
			});
	}
})();
