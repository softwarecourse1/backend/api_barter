/*jshint esversion: 6 */

const bcrypt = require('bcrypt');
const Controller = require(`${config.path.controller}/Controller`);
const UserTransform = require(`${config.path.transform}/V1.0/UserTransform`);

module.exports = new (class AuthController extends Controller {
	register(req, res) {
		// Validation
		req.checkBody('email', 'لطفا ایمیل صحیح وارد کنید.').isEmail();
		req.checkBody('username', 'لطفا ایمیل صحیح وارد کنید.').notEmpty();
		req.checkBody('mobile', 'لطفا ایمیل صحیح وارد کنید.').notEmpty();
		req.checkBody('password', 'لطفا پسورد را وارد کنید.').notEmpty();

		// Show errors
		if (this.showValidationErrors(req, res)) {
			return;
		}

		this.model
			.User({
				username: req.body.username,
				email: req.body.email,
				password: req.body.password,
				mobile: req.body.mobile
			})
			.save(err => {
				if (err) {
					if (err.code == 11000) {
						return res.status(402).json({
							data: 'duplicate email',
							success: false
						});
					}
				}

				res.status(200).json({
					//body: 
					data: 'register successfull',
					success: true
				});
			});
		// Create user
	}

	login(req, res) {
		req.checkBody('email', 'لطفا ایمیل صحیح وارد کنید.').isEmail();
		req.checkBody('password', 'لطفا پسورد را وارد کنید.').notEmpty();

		// Show errors
		if (this.showValidationErrors(req, res)) {
			return;
		}

		this.model.User.findOne({ email: req.body.email }, (err, user) => {
			if (err) throw err;

			if (user == null) {
				return res.status(422).json({
					data: 'not exist',
					success: false
				});
			}
			bcrypt.compare(req.body.password, user.password, (err, status) => {
				if (status) {
					res.status(200).json({
						body:[new UserTransform().transform(user, true)]
						
						
					});
				} else {
					res.status(422).json({
						data: 'not exist',
						success: false
					});
				}
			});
		});
	}

	editProfile(req, res) {
		req.checkBody('mobile', 'لطفا پسورد را وارد کنید.').notEmpty();
		req.checkBody('email', 'لطفا ایمیل صحیح وارد کنید.').isEmail();

		// Show errors
		if (this.showValidationErrors(req, res)) {
			return;
		}

		this.model.Users.findOneAndUpdate(req.body.email, { mobile: req.body.mobile }, (err, course) => {
			res.json('update succesfull');
		});
	}
})();
