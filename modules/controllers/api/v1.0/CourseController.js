/*jshint esversion: 6 */

const Controller = require(`${config.path.controller}/Controller`);
const CourseTransform = require(`${config.path.transform}/V1.0/CourseTransform`);

module.exports = new (class CourseController extends Controller {
	index(req, res) {
		const page = req.query.page || 1;
		this.model.Course.paginate({}, { page, limit: 2, populate: ['episodes'] })
			.then(result => {
				if (result) {
					return res.json({
						// data: {
						// 	items: new CourseTransform().withEpisodes().transformCollection(result.docs),
						// 	limit: result.limit,
						// 	page: result.page,
						// 	pages: result.pages,
						// 	total: result.total
						// },
						data: new CourseTransform()
							.withPaginate()
							.withEpisodes()
							.transformCollection(result),
						success: true
					});
				} else {
					res.json({
						message: 'course empty',
						success: false
					});
				}
			})
			.catch(err => console.log(err));

		// this.model.Course.find({})
		// 	.populate('episodes')
		// 	.exec((err, courses) => {
		// 		if (err) throw err;

		// 		if (courses) {
		// 			return res.json({
		// 				data: new CourseTransform().withEpisodes().transformCollection(courses),
		// 				success: true
		// 			});
		// 		} else {
		// 			res.json({
		// 				message: 'course empty',
		// 				success: false
		// 			});
		// 		}
		// 	});

		// this.model.Course.find({}, (err, courses) => {
		// 	if (err) throw err;

		// 	if (courses) {
		// 		return res.json({
		// 			data: new CourseTransform().transformCollection(courses),
		// 			success: true
		// 		});
		// 	} else {
		// 		res.json({
		// 			message: 'course empty',
		// 			success: false
		// 		});
		// 	}
		// });

		// this.model.Course.findOne({ _id: '5e3c49d59cd5ad03c3806c32' }, (err, course) => {
		// 	if (err) throw err;
		// 	if (course) {
		// 		res.json({
		// 			data: new CourseTransform().transform(course),
		// 			success: true
		// 		});
		// 	} else {
		// 	}
		// });
	}
})();
