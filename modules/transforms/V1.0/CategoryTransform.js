/*jshint esversion: 6 */

const Transform = require('../Transform');

module.exports = class CategoryTransform extends Transform {
    transform(item) {
		return {
			id: item.id,
			title: item.title,
			icon: item.icon
		};
	}
};
