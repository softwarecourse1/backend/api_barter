/*jshint esversion: 6 */

const Transform = require('../Transform');

module.exports = class CourseTransform extends Transform {
	transform(item) {
		return {
			title: item.title,
			body: item.body,
			price: item.price,
			...this.showEpisodes(item)
		};
	}

	withEpisodes() {
		this.withEpisodesStatus = true;
		return this;
	}

	showEpisodes(item) {
		const episodeTransform = require('./EpisodeTransform');

		if (this.withEpisodesStatus) {
			return {
				episodes: new episodeTransform().transformCollection(item.episodes)
			};
		} else {
			return {};
		}
	}

	CollectionName() {
		return 'courses';
	}
};
