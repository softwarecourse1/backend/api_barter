/*jshint esversion: 6 */

const Transform = require('../Transform');

module.exports = class CityTransform extends Transform {
    transform(item) {
		return {
			id: item.id,
			title: item.title
		};
	}
};
