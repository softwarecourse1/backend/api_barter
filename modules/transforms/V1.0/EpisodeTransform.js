/*jshint esversion: 6 */

const Transform = require('../Transform');
const CourseTransform = require(`./CourseTransform`);

module.exports = class EpisodeTransform extends Transform {
	// this.withEpisodesStatus=false

	transform(item) {
		return {
			title: item.title,
			body: item.body,
			...this.showCourseItem(item)
		};
	}

	withCourse() {
		this.withCourseStatus = true;
		return this;
	}

	showCourseItem(item) {
		if (this.withCourseStatus) {
			return {
				course: new CourseTransform().transform(item.course)
			};
		}
		return {};
	}
};
