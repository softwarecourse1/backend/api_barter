/*jshint esversion: 6 */

module.exports = class Transform {
	transformCollection(items) {
		if (this.withPaginateStatus) {
			return {
				[this.CollectionName()]: items.docs.map(this.transform.bind(this)),
				...this.paginateItems(items)
			};
		}
		return items.map(this.transform.bind(this));
	}

	withPaginate() {
		this.withPaginateStatus = true;
		return this;
	}

	CollectionName() {
		return 'items';
	}

	paginateItems(items) {
		return {
			limit: items.limit,
			page: items.page,
			pages: items.pages,
			total: items.total
		};
	}
};
