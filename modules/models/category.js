/*jshint esversion: 6 */

const bcrypt = require('bcrypt');
const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');

const schema = mongoose.Schema;

const schemaCategory = new schema({
	id: { type: String, required: true },
	title: { type: String, required: true},
	icon: {type: String , required: true},
});

schemaCategory.plugin(timestamps);


module.exports = mongoose.model('Category', schemaCategory);
