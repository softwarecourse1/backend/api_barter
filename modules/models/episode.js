/*jshint esversion: 6 */

const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');
const mongoosePaginate = require('mongoose-paginate');

const schema = mongoose.Schema;

const schemaEpisode = new schema({
	course: { type: schema.Types.ObjectId, ref: 'Course' },
	title: { type: String, required: true },
	body: { type: String, required: true },
	videoUrl: { type: String, required: true },
	number: { type: String, required: true },
	viewCount: { type: Number, default: 0 },
	commentCount: { type: Number, default: 0 }
});

schemaEpisode.plugin(timestamps);
schemaEpisode.plugin(mongoosePaginate);

module.exports = mongoose.model('Episode', schemaEpisode);
