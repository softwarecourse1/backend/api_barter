/*jshint esversion: 6 */

const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');
const Schema = mongoose.Schema;

const OfferSchema = new Schema({
	offer_cat_id: { type: String, required: true },
	offer_description: { type: String, required: true },
	offer_title: { type: String, required: true },
	offer_image_id: { type: String, required: true },
	offer_location_id: { type: String, required: true },
	offer_username: { type: String, required: true },
    offer_pairs : [{ type : Schema.Types.ObjectId , ref : 'Pair'}]
});
OfferSchema.plugin(timestamps);

module.exports = mongoose.model('Offer', OfferSchema);
