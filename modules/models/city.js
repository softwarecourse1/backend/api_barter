/*jshint esversion: 6 */

const bcrypt = require('bcrypt');
const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');
const schema = mongoose.Schema;

const schemaCity = new schema({
	id: { type: String, required: true },
	title: { type: String, required: true },
});


schemaCity.plugin(timestamps);

module.exports = mongoose.model('City', schemaCity);
