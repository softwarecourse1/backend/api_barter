const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const timestamps = require('mongoose-timestamp');

const PairSchema = new Schema({
    cat_id : { type : String , required : true} ,
    pair_title : { type : String , required : true} ,
    pair_advantage : { type : String , required : true}
});

PairSchema.plugin(timestamps);

module.exports = mongoose.model('Pair' , PairSchema);