/*jshint esversion: 6 */

const bcrypt = require('bcrypt');
const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');
const schema = mongoose.Schema;

const schemaUser = new schema({
	username: { type: String, required: true },
	email: { type: String, required: true, unique: true },
	mobile: {type: String , required: true},
	password: { type: String, required: true },
});

schemaUser.plugin(timestamps);

schemaUser.pre('save', function(next) {
	console.log(this);
	bcrypt.hash(this.password, 14, (err, hash) => {
		this.password = hash;
		next();
	});
});

module.exports = mongoose.model('User', schemaUser);
