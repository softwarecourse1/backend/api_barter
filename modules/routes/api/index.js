const express = require('express');
const router = express.Router();

const apiV1_0 = require('./api-v1-0');
const apiV1_2 = require('./api-v1-2');

router.use('/v1', apiV1_0);
router.use('/v1.2', apiV1_2);

module.exports = router;
