const express = require('express');
const apiRouter = express.Router();

apiRouter.get('/', (req, res) => {
	res.json('Welcome to API');
});

apiRouter.get('/courses', (req, res) => {
	res.json({
		data: [
			{
				title: 'course 1',
				content: 'Course 1 body'
			},
			{
				title: 'course 2',
				content: 'Course 2 body'
			}
		]
	});
});

module.exports = apiRouter;
