/*jshint esversion: 6 */

const express = require('express');
const router = express.Router();

// Middlewares
const apiAuth = require('./middleware/apiAuth');
const apiAdmin = require('./middleware/apiAdmin');
const { uploadImage } = require('./middleware/upload');

// Controllers
const { api: ControllerAPI } = config.path.controllers;
const controllerAdminCourse = require(`${ControllerAPI}/v1.0/admin/CourseController`);
const controllerAdminEpisode = require(`${ControllerAPI}/v1.0/admin/EpisodeController`);
const controllerHomePageData = require(`${ControllerAPI}/v1.0/HomePageDataController`);
const controllerOffer = require(`${ControllerAPI}/v1.0/offerController`);

const controllerHome = require(`${ControllerAPI}/v1.0/HomeController`);
const controllerCourse = require(`${ControllerAPI}/v1.0/CourseController`);
const controllerEpisode = require(`${ControllerAPI}/v1.0/EpisodeController`);
const controllerAuth = require(`${ControllerAPI}/v1.0/AuthController`);
const controllerUser = require(`${ControllerAPI}/v1.0/UserController`);

router.get('/', controllerHome.index);
router.get('/version', controllerHome.version);
router.get('/courses', controllerCourse.index.bind(controllerCourse));
router.get('/episodes/:id', controllerEpisode.single.bind(controllerEpisode));
router.post('/login', controllerAuth.login.bind(controllerAuth));
router.post('/register', controllerAuth.register.bind(controllerAuth));
router.put('/profile', controllerAuth.editProfile.bind(controllerAuth));
router.get('/user', apiAuth, controllerUser.index.bind(controllerUser));
router.post('/user/image', apiAuth, uploadImage.single('image'), controllerUser.uploadImage.bind(controllerUser));

router.post('/registerCategory',apiAuth,controllerHomePageData.registerCategory.bind(controllerHomePageData));
router.post('/registerCity',apiAuth,controllerHomePageData.registerCity.bind(controllerHomePageData));

router.post('/registerOffer',apiAuth,controllerOffer.registerOffer.bind(controllerOffer));


router.get('/getCities',apiAuth,controllerHomePageData.getCities.bind(controllerHomePageData));
router.get('/getCategory',apiAuth,controllerHomePageData.getCategory.bind(controllerHomePageData));

const adminRouter = express.Router();
adminRouter.get('/courses', apiAuth, controllerAdminCourse.index.bind(controllerAdminCourse));
adminRouter.get('/courses/:id', apiAuth, controllerAdminCourse.single.bind(controllerAdminCourse));
adminRouter.post('/courses', apiAuth, controllerAdminCourse.store.bind(controllerAdminCourse));
adminRouter.put('/courses/:id', apiAuth, controllerAdminCourse.update.bind(controllerAdminCourse));
adminRouter.delete('/courses/:id', apiAuth, controllerAdminCourse.destroy.bind(controllerAdminCourse));

adminRouter.get('/episods', apiAuth, controllerAdminEpisode.index.bind(controllerAdminEpisode));
adminRouter.get('/episods/:id', apiAuth, controllerAdminEpisode.single.bind(controllerAdminEpisode));
adminRouter.post('/episods', apiAuth, controllerAdminEpisode.store.bind(controllerAdminEpisode));
adminRouter.put('/episods/:id', apiAuth, controllerAdminEpisode.update.bind(controllerAdminEpisode));
adminRouter.delete('/episods/:id', apiAuth, controllerAdminEpisode.destroy.bind(controllerAdminEpisode));

//adminRouter.get('/test',apiAuth,controllerHomePageData.index.bind(controllerHomePageData));


router.use('/admin', apiAuth, apiAdmin, adminRouter);

module.exports = router;
