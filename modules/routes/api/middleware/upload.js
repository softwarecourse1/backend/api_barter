/*jshint esversion: 6 */

const mkdirp = require('mkdirp');
const multer = require('multer');

const storageImage = multer.diskStorage({
	destination: (req, file, cb) => {
		let year = new Date().getFullYear();
		let month = new Date().getMonth() + 1;
		let day = new Date().getDay();
		let path = `./public/uploads/images/${year}/${month}/${day}`;
		mkdirp(path).then(made => cb(null, path));
	},
	filename: (req, file, cb) => {
		cb(null, Date.now() + '-' + file.originalname);
	}
});
const imageFilter = (req, file, cb) => {
	if (file.mimetype === 'image/png' || file.mimetype === 'image/jpeg') {
		cb(null, true);
	} else {
		cb(null, false);
	}
};
const uploadImage = multer({
	storage: storageImage,
	fileFilter: imageFilter,
	limits: {
		fileSize: 1024 * 1024 // 1mb
	}
});

module.exports = {
	uploadImage
};
