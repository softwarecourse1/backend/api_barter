/*jshint esversion: 6 */

const jwt = require('jsonwebtoken');
const User = require(`${config.path.model}/user`);

module.exports = (req, res, next) => {
	let token = req.body.token || req.query.token || req.headers['x-access-token'];
	if (token) {
		return jwt.verify(token, config.secret, (err, decode) => {
			if (err) {
				return res.status(404).json({
					data: 'Auth failed',
					successfull: false
				});
			}
			console.log(decode.user_id);
			User.findById(decode.user_id, (err, user) => {
				if (err) throw err;

				if (user) {
					user.token = token;
					req.user = user;
					next();
					return;
				}
				return res.status(404).json({
					data: 'user not found',
					successfull: false
				});
			});
		});
	}
	return res.status(403).json({
		data: 'no token provided',
		successfull: false
	});
};
