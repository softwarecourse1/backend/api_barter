/*jshint esversion: 6 */

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');
const mongoose = require('mongoose');

global.config = require('./modules/config');

// Connect to DB
mongoose.connect('mongodb://127.0.0.1:27017/NodeJSApi', {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false,
	useCreateIndex: true
});
mongoose.Promise = global.Promise;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({ type: 'application/json' }));
app.use(expressValidator());
app.use('/public', express.static('public'));

const webRouter = require('./modules/routes/web');
const apiRouter = require('./modules/routes/api');

app.use('/', webRouter);
app.use('/api', apiRouter);

app.listen(config.port, () => {
	console.log(`Server running at ${config.port}`);
});
